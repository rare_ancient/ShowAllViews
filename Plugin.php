<?php
if (!defined('__TYPECHO_ROOT_DIR__')) exit;
/**
 * ShowAllViews 是一个便于你展示你所有文章阅读量的插件。
 * 
 * @package ShowAllViews
 * @author 吃猫的鱼
 * @version 1.0.0
 * @link https://www.fish9.cn
 */

 /**
  * 本插件作者为吃猫的鱼
  * 为开源插件，未经许可，不允许任何个体/企业对其进行二次开发/售卖！
  */
 
 class ShowAllViews_Plugin implements Typecho_Plugin_Interface
{
 /* 激活插件方法 */
public static function activate(){
    Typecho_Plugin::factory('usr/themes/index.php')->allview = array('ShowAllViews_Plugin', 'render');
    return '插件开启成功';
}
 
/* 禁用插件方法 */
public static function deactivate(){
    return '插件关闭成功';
}



/* 插件配置方法 */
public static function config(Typecho_Widget_Helper_Form $form){
    $ViewsKey = new Typecho_Widget_Helper_Form_Element_Text('views', NULL, '', _t('数据库中访问数的字段名 <a href="https://www.fish9.cn/archives/408/">点我查看教程</a>'));
    $form->addInput($ViewsKey);
}
 
/* 个人用户的配置方法 */
public static function personalConfig(Typecho_Widget_Helper_Form $form){}
 
/* 插件实现方法 */
public static function render(){
    $db = Typecho_Db::get();
    $query= $db->select('sum('.Typecho_Widget::widget('Widget_Options')->plugin('ShowAllViews')->views.') views' )->from('typecho_contents');
    $result = $db->fetchAll($query);
?>
<script>
    document.getElementById("fishviews").innerHTML = "<?php echo $result[0]['views']; ?>";
</script>

<?php
}
}

?>