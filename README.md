# ShowAllViews For typecho
作者：吃猫的鱼
博客地址：[吃猫的鱼个人博客](https://www.fish9.cn)
#### 介绍
本插件是一个便于在typecho中显示你所有文章阅读数的插件！

#### 安装教程
1. 将插件上传到typecho插件目录中， **插件名字改成``ShowAllViews``** 。
2. 设置插件，在插件中填写你的数据表"typecho_contents"中的文章阅读数量的字段，自己看看大概是哪个...（**typecho本身并没有该字段，可能只有部分主题有，本插件仅支持那些有该字段的主题使用！**）
3. 在你想要添加显示的位置中加入下方代码（一定要是php文件！），样式请自行更改设计，需要显示阅读量的标签的id必须为``fishviews``
```php
<div>文章累计阅读量：<a id="fishviews">0</a><div>
<?php \Typecho\Plugin::factory('usr/themes/index.php')->allview(); ?>
```

#### 最后
我的个人博客：https://www.fish9.cn